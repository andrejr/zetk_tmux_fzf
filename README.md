# zetk_fzf

A set of `fzf`-based shell scripts to easily interface with
[`zetk`](https://gitlab.com/andrejr/zetk).

[![asciicast](https://asciinema.org/a/505823.svg)](https://asciinema.org/a/505823)

## What?

This set of scripts and configs provides an easy to use terminal
interface for [`zetk`](https://gitlab.com/andrejr/zetk) which integrates
with your editor, `tmux`, `fzf`, `bat`, and other tools.

Unlike [`zetk`](https://gitlab.com/andrejr/zetk), this toolset isn't
packaged because it's not a rigid program distribution. It's more of a trivial
set of scripts you'll tailor to your needs anyway.

### What's inside?

This set of helpers includes some scripts, a `systemd` user unit for
`zk-daemon`, a [`tmuxp`](https://github.com/tmux-python/tmuxp) config, and a
couple of useful additions to your `.vimrc`/`init.vim`.

## How to install

### Requirements

This set of scripts requires you to have `bash`, `tmux`, `fzf`, `bat`, `nvim`
or `vim`, `tmuxp` and `python` (+ `httpx`, for the `zk-assets-localize` script)

### Installation

- scripts (`zkn`, `zks`, `zkt`, `zk-assets-localize`), the supporting
  `lib` folder (which must be in the same folder as the scripts):
  install these into a folder on your path.  
  I keep them in `~/zk`, and just add `~/zk` to my `PATH` in `.zshenv`

- set your environment variables to point to your zettelkasten; I put the
  following into my `.zshenv`, it's similar for Bash.

  ```sh
   export ZK_PATH=~/zettelkasten
   # optional, see https://gitlab.com/andrejr/zetk#configuration
   export ZK_STOPWORDS="$ZK_PATH/stopwords"
  ```

- systemd service for the daemon: put `systemd/zk-daemon.service` into
  `~/.config/systemd/user` and activate with `systemd --user enable --now
  zk-daemon`; see
  [this](https://gitlab.com/andrejr/zetk#general-notes-on-operation) for more
  info on the daemon

- [`tmuxp`](https://github.com/tmux-python/tmuxp) config file
  (`tmuxp/zk.yaml`): copy it to `~/.tmuxp`

- put this into your `.vimrc`/`init.vim` to get simple
  *new note* functionality
    ```viml
    function! s:NoteCommand(...) abort
        let zk_path = $ZK_PATH
        let title = trim(join(a:000))
        let path = zk_path . '/'
                    \ . strftime("%Y%m%d%H%M")
                    \ . " "
                    \ . title . ".md"

        execute ":e " . fnameescape(path)

        let newfile = ! filereadable(expand(path))
        if newfile
            call append(0, '# ' . title)
        endif

    endfunction

    " Adds a new note to your zettelkasten (opens it for editing)
    command! -nargs=* Note call s:NoteCommand(<f-args>)
  ```
  Now typing `:Note some title` creates a new note in your zettelkasten

## Usage

The scripts provided are meant to be used in a `tmux` session, in which the
left window (split) hosts `nvim`, and the right one hosts `zks` or `zkt`.
The provided `tmuxp` config file gives you such a layout. The rest of this
guide assumes you're using the tools as a part of this setup.

# Selector scripts

The `zks` and `zkt` scripts work similarly: `zks` allows you to fulltext search
your zettelkasten, while `zkt` allows you to fuzzy search the tags. You may
switch between them with `Alt-f`. Pressing `Alt-c` opens up a context menu, and
each option opens a different fzf selector. There are keyboard shortcuts for
opening files and inserting tags/filenames into NeoVim.

The selectors behave differently based on which kind of items (files or tags)
they're selecting: both the context menu options, and keyboard shortcuts
differ. An explanation follows.


#### File selectors

These shortcuts and the context menu are only available when selecting files.
When selecting files, the preview shows your file displayed with `bat`.

##### Keyboard shortcuts

- `Enter` - opens the selected file in `nvim` (`:e <selected_file>`)
- `Ctrl-o` - inserts the link to currently selected file in vim, `[[link]]`
  style
- `Ctrl-i` - inserts the link to currently selected file in vim, `[link](link)`
  style
- `Ctrl-v` - opens the selected file in `nvim`, in a vertical split (`:vsp
  <selected_file>`)
- `Ctrl-s` - opens the selected file in `nvim`, in a horizontal split (`:sp
  <selected_file>`)
- `Ctrl-t` - opens the selected file in `nvim`, in a new tab (`:tabnew
  <selected_file>`)
- `Alt-c` - opens the context menu

##### Context menu options
- `lnk` - list files linked to in file
- `blnk` -list files back-linking to file
- `sim` - list similar files (similar text)
- `simt` - list similar files (similar tags)
- `gv` - open neighborhood graph (graphical, links only) in browser
- `gvt` - open neighborhood graph (graphical, links and tags) in browser
- `3` - neighborhood tree selector
- `3t` - neighborhood tree selector, common tag connections
- `tag` - list all tags in file

#### Tag selectors

These shortcuts and the context menu are only available when selecting tags.
When selecting tags, the preview window lists all files the tag appears in.

##### Keyboard shortcuts

- `Enter` - opens a list of all files containing that tag
- `Ctrl-o`, `Ctrl-i` - inserts the current tag in vim
- `Alt-c` - opens the context menu

##### Context menu options
- `rel` - list related tags - tags coexisting with tag in documents
- `gvt` - neighborhood graph (graphical, links and tags)

### Auxilary scripts

`zkn` creates a new note and opens it in `neovim`.

`zkg` generates an `.svg` file with the link/tag (with -t) graph and opens it
in your browser, it's a simple wrapper for `zk-dot` and forwards all args to
it.

`zk-assets-localize` downloads all images linked to in documents, adds them to
"$ZK_PATH/media" and changes the files to point to them.

## Vim

You can use `:Note some title` to create a new note in your zettelkasten.

## Contributing

Bugfixes and reports welcome. This is a pile of trivial scripts, any attempts
to make them more robust is welcome. POSIXification is also desireable, I used
Bash because I'm lazy. Any changes similar in spirit are also desireable.
