#!/usr/bin/env bash
# -*- coding: utf-8 -*-

set -o errexit
set -o pipefail
set -o noclobber
set -o nounset

file="$1"
query="$2"

if [ -z "$query" ]; then
	cat "$ZK_PATH/$file"
else
	[ -n "$file" ] && zk-fts -f "$file" "$query"
fi | bat --language md --style=plain --color always
