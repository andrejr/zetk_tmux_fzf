#!/usr/bin/env bash
# -*- coding: utf-8 -*-

set -o errexit
set -o pipefail
set -o noclobber

query="$1"

if [ -z "$query" ]; then
    find "$(readlink -f "$ZK_PATH")" -iname '*.md' -printf "%T@\t%f\0" \
		| sort --zero-terminated --numeric-sort \
		| cut --zero-terminated --delimiter=$'\t' --fields=2-
else
	zk-fts --print0 "$query" || true
fi
