#!/usr/bin/env bash
# -*- coding: utf-8 -*-

set -o errexit
set -o pipefail
set -o noclobber
set -o nounset

script_dir=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &> /dev/null && pwd -P)
lib_dir="$script_dir"

source "$lib_dir/common.sh"

tag="$1"

tag_picker "zk-rel-tags --print0 \"$tag\"" \
	"Tags coexisting with '$tag' in documents"
