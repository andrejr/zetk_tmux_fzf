#!/usr/bin/env bash
# -*- coding: utf-8 -*-

set -o errexit
set -o pipefail
set -o noclobber
set -o nounset

script_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" &> /dev/null && pwd -P)
lib_dir="$script_dir"
script_path="${BASH_SOURCE[0]}"

# echo $script_path

# nl=$'\n'
hl=$'\033[93m'
nohl=$'\033[39m'

choices=$(
	cat - << EOF
${hl}rel${nohl} related tags - tags coexisting with tag in documents
${hl}gvt${nohl} neighborhood graph (graphical, links and tags)
EOF
)

file="$1"

if [[ -z ${2:-} ]]; then
	echo "$choices" \
		| fzf \
			--ansi \
			--nth=1 \
			--no-multi \
			--cycle \
			--bind "enter:execute[$script_path \"$file\" {}]"
else
	choice="$(echo "$2" | awk '{ print $1; }')"
	case $choice in
		rel)
			"$lib_dir/rel_tags.sh" "$1"
			;;
		gvt)
			"$lib_dir/../zkg" -t "$1"
			;;
		*)
			echo unknown command
			echo "$2"
			false
			;;
	esac
	echo "$file"

fi
