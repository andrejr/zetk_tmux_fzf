#!/usr/bin/env bash
# -*- coding: utf-8 -*-

set -o errexit
set -o pipefail
set -o noclobber
set -o nounset

script_dir=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &> /dev/null && pwd -P)
lib_dir="$script_dir"

source "$script_dir/common.sh"

wrapped_args=''
for arg in "$@"
do
    wrapped_args="$wrapped_args'$arg' "
done


tree_picker "zk-tree $wrapped_args" \
	"Link tree for '$1'"
