#!/usr/bin/env bash
# -*- coding: utf-8 -*-

set -o errexit
set -o pipefail
set -o noclobber
set -o nounset

script_dir=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &> /dev/null && pwd -P)
lib_dir="$script_dir"

source "$script_dir/common.sh"

target="$(lstrip "$1")"

if is_file "$target"; then
	"$lib_dir/file_ctx_menu.sh" "$target"
else
	"$lib_dir/tag_ctx_menu.sh" "$target"
fi
