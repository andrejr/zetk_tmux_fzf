#!/usr/bin/env bash
# -*- coding: utf-8 -*-

set -o errexit
set -o pipefail
set -o noclobber
set -o nounset

script_dir=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &> /dev/null && pwd -P)
lib_dir="$script_dir"

source "$script_dir/common.sh"

lsarg=$(lstrip "$1")

if is_file "$lsarg"; then
	"$lib_dir/fts_preview_wrapper.sh" "$lsarg" "${2:-""}"
else
	zk-fts --tags "${lsarg#\#}"
fi
