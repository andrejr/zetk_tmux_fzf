#!/usr/bin/env bash
# -*- coding: utf-8 -*-

set -o errexit
set -o pipefail
set -o noclobber
set -o nounset

script_dir=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &> /dev/null && pwd -P)
lib_dir="$script_dir"

source "$script_dir/common.sh"

action="$1"
target="$(lstrip "$2")"

if is_file "$target"; then
	"$lib_dir/tmux.sh" "$action" "$target"
else
	case $action in
		i | l)
			"$lib_dir/tmux.sh" "$action" "$target"
			;;
		*) ;;

	esac
fi
